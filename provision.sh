#!/bin/bash

# Установка Docker

sudo apt-get update
sudo apt-get install -y \
ca-certificates \
curl \
gnupg \
lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Установка Gitlab Runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
sudo usermod -aG docker gitlab-runner
sleep 5
# Создание проекта в Gitlab и добавление раннеров
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$key_gitlab" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "test" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$key_gitlab" \
  --executor "shell" \
  --description "test" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"



